//
//  MapView.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/3.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit
import MapKit

class MapView: MKMapView, MKMapViewDelegate {
    
    var usersLocation:CLLocation? {
        didSet {
            if oldValue == nil {
//                animateMapViewFor(coordinate: location?.coordinate)
            }
        }
    }
    var vc:ViewController?
    var pressedLocation: CLLocation?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate = self
        showsBuildings = false
        showsPointsOfInterest = false
        showsScale = false
        showsCompass = false
        showsTraffic = false
        mapType = .mutedStandard
        showsUserLocation = true
        self.tintColor = .orange
    }
    
    /*
     Delta是缓冲区，值越小，地图密度越高
    */
    func animateMapViewFor(coordinate:CLLocationCoordinate2D?, delta:CLLocationDegrees) {
        guard let coordinate = coordinate else {
            return
        }
        let span = MKCoordinateSpan(latitudeDelta: delta, longitudeDelta: delta)
        let reg = self.regionThatFits(MKCoordinateRegion(center: coordinate,
                                                            span: span))
        self.setRegion(reg, animated: true)
    }

    
    
    //MARK: mapview delegate
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        if fullyRendered == true {
            
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        self.usersLocation = userLocation.location
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay.title == "runTime"{
            DataManager.shared.runtimePolyLines.append(overlay)
        }
        
//        if overlay.title == "poly" {
//            let ren = MKPolygonRenderer(overlay: overlay)
//            ren.fillColor = .yellow
//            ren.alpha = 0.5
//            return ren
//        }
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .orange
        renderer.lineWidth = 4
        return renderer
    }
    
    
    @IBAction func longPress(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            print("began")
            if (vc?.userState == .recording) {
                return
            }
            let coord = self.convert(sender.location(ofTouch: 0, in: self), toCoordinateFrom: self)
            
            if DataManager.shared.startDate == nil {
                DataManager.shared.startDate = Date()
            }
            let newLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            if self.pressedLocation != nil {
                DataManager.shared.runtimeDistance = newLocation.distance(from: self.pressedLocation!)
            }
            self.pressedLocation = CLLocation(latitude: coord.latitude, longitude: coord.longitude)
            DataManager.shared.runtimeGroupNames.insert("手动添加的路线" ,at:0)
            LocationManager.shared.getGeoCodeOfUser(block: {
                DataManager.shared.insertData(locationPlaceMark: LocationManager.shared.userPlaceMark, coordinate: coord, isFixedCoordinate: LocationManager.shared.isNeedFixedCoordinateForChina, mapview: self)
            }, location: CLLocation(latitude: coord.latitude, longitude: coord.longitude))
            
            break
        case .cancelled:
//            print("cancelled")
            break
        case .changed:
//            print("changed")
            break
        case .ended:
//            print("ended")
            break
        case.failed:
//            print("failded")
            break
        case .possible:
//            print("Possible")
            break
        default:
            print("press default")
            break
        }
    }
    
    
    func addPolyLinesFrome(coordinatesSection:[[CLLocationCoordinate2D]]) {
        for var coordinates in coordinatesSection {
            let polyLines = MKPolyline(coordinates: &coordinates, count: coordinates.count)
            addOverlay(polyLines)
            DataManager.shared.polylinesAddedOnMapsFromLoadingRoute.append(polyLines)
        }
    }
    
    
    func removePolyLinesWith(coordinatesSection:[[CLLocationCoordinate2D]]) {
        for var coords in coordinatesSection {
            let polyLines = MKPolyline(coordinates: &coords, count: coords.count)
            removeOverlays([polyLines])
        }
    }
    
//    func removePolyGonsWith(coordinatesSection:[[CLLocationCoordinate2D]]) {
//        for var coords in coordinatesSection {
//            let polyLines = MKPolygon(coordinates: &coords, count: coords.count)
//            removeOverlays([polyLines])
//        }
//    }
    
    func setMapRectForShare() {
        var coords = DataManager.shared.runtimeCoordinates
        let poly = MKPolygon(coordinates: &coords, count: coords.count)
//        poly.title = "poly"
        vc?.mapView.setVisibleMapRect(poly.boundingMapRect, edgePadding: UIEdgeInsets(top: 100, left: 30, bottom: 120, right: 30), animated: true)
//        vc?.mapView.addOverlay(poly)
        //
    }
    
    func setMapToCenter() {
        var coord = DataManager.shared.loadedCoordinates2DSections.flatMap({$0})
        let poly = MKPolygon(coordinates: &coord, count: coord.count)
        vc?.mapView.setVisibleMapRect(poly.boundingMapRect, edgePadding: UIEdgeInsets(top: 100, left: 30, bottom: 120, right: 30), animated: true)
    }
}



extension MapView {
//    func snapShot(block: @escaping ()->()) {
//        let options = MKMapSnapshotter.Options()
//        options.showsBuildings = false
//        options.showsPointsOfInterest = false
//        options.region = self.region
//        let shotter = MKMapSnapshotter(options: options)
//        shotter.start { (snapshot, err) in
//            if let err = err {
//                print(err)
//            } else {
////                print("Image:",snapshot.image)
//                DispatchQueue.main.async {
//                    //Do Something here
//                    DataManager.shared.runtimeRouteImage = self.takeScreenshot(false)//snapshot.image
//                    block()
//                }
//            }
//        }
//    }
    
//    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
//        var screenshotImage :UIImage?
//        var layer = CALayer()
//        layer = self.layer//UIApplication.shared.keyWindow!.layer
//        let scale = UIScreen.main.scale
////        let rect = layer.frame.inset(by: UIEdgeInsets(top: 40, left: 20, bottom: 40, right: 20))
//        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale);
//        guard let context = UIGraphicsGetCurrentContext() else {return nil}
//        layer.render(in:context)
//        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        if let image = screenshotImage, shouldSave {
//            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//        }
//        return screenshotImage
//    }
}





class CustomPin:NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
    
    
}
