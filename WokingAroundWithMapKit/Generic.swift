//
//  Generic.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/13.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class Generic: NSObject {
    static let shared = Generic()
    

}


extension Date {
    func stringWith(timeStyle:DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = timeStyle
        dateFormatter.locale =  Locale(identifier: Locale.preferredLanguages.first ?? "en")
        return dateFormatter.string(from: self)
    }
}

extension Double {
    
    var formattedString: String {
        if self > 100 {
            return String(format: "%.1f km", self / 1000)
        }
        return String(format: "%.1f m", self)
    }
    
}

extension CLLocation {
    var fixed:CLLocation {
        let fixedCoord =   LPXCoordinateTransfromUtil.gcj02fromwgs84(coordinate)
        return CLLocation(coordinate: fixedCoord,
                                   altitude: altitude,
                                   horizontalAccuracy: horizontalAccuracy,
                                   verticalAccuracy: verticalAccuracy,
                                   course: course,
                                   speed: speed,
                                   timestamp: timestamp)
    }
}
