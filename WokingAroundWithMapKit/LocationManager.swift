//
//  LocationManager.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/3.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class LocationManager: CLLocationManager, CLLocationManagerDelegate {
    static let shared = LocationManager()
    
    var lastLocation:CLLocation? {
        didSet {
            if oldValue == nil {
//                vc.mapView.animateMapViewFor(coordinate: lastLocation?.coordinate, delta: 0.05)
            } else {
                if oldValue != nil {
                    distance = oldValue!.distance(from: lastLocation!)
                    if vc.userState == .recording {
                        DataManager.shared.runtimeDistance +=  distance
                    }
                }
            }
        }
    }
    
    
    var currenLocation:CLLocation {
        get {
            if isNeedFixedCoordinateForChina {
                if let lastRaw = lastLocation {
                    return lastRaw.fixed
                }
            }
            return lastLocation ?? CLLocation()
        }
    }
    
    var centerOfFistLocatin:CLLocation? {
        didSet {
            if oldValue == nil {
                DataManager.shared.prepareDataWith(locationPlaceMark: userPlaceMark)
                DataManager.shared.resetForStop()
            }
        }
    }
    
    var cityPlaceMark:CLPlacemark? {
        didSet {
            
        }
    }
    
    var userPlaceMark:CLPlacemark? {
        didSet {
            vc.updateDebugView(calledFrom: "User Place Mark Update")
            if shouldUpdateUserPlaceMark != false {
                updateTime = Date().timeIntervalSinceReferenceDate
                print("时间到了，User Place Mark 已更新")
            }
        }
    }
    
    var isNeedFixedCoordinateForChina:Bool{
        get {
            if userPlaceMark?.isoCountryCode == "CN" {
                return true
            } else {
                return false
            }
        }
    }
    
    var shouldUpdateUserPlaceMark:Bool {
        get {
            return Date().timeIntervalSinceReferenceDate - updateTime >= 300
        }
    }
    
    var isReady:Bool {
        get {
            if lastLocation == nil {
                return false
            } else {
                return true
            }
        }
    }
    
    //did update Location 的时候实时更新
    var course : CLLocationDirection? {
        didSet {
            vc.updateDebugView(calledFrom: "Course Update")
        }
    }
    
    //记录最后一次方向的变化
    var lastCourse:CLLocationDirection?
    
    
    //用来初始化到所在城市的中心
    var centerOflocality:CLLocation?
    var vc:ViewController!
    var updateTime = TimeInterval()
    var distance = Double()
    
    override init() {
        super.init()
        self.delegate = self
        allowsBackgroundLocationUpdates = true
        pausesLocationUpdatesAutomatically = true
    }
    
    
    func setAccuracyForActive() {
        desiredAccuracy = kCLLocationAccuracyBest
        stopMonitoringSignificantLocationChanges()
        print("Did set setAccuracy For Active()")
    }
    
    func setAccuracyForBackGround() {
        desiredAccuracy = kCLLocationAccuracyBest
        startMonitoringSignificantLocationChanges()
        print("Did set setAccuracy For BackGround()")
    }
    
    func checkAuth() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .denied,.restricted:
            print("没有权限")
            //TODO:这里需要强烈的提示
        case .authorizedAlways:
            self.startUpdatingLocation()
        case .authorizedWhenInUse:
            //需要提示要开启使用允许
            self.startUpdatingLocation()
        default:
            requestAlwaysAuthorization()
            requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            startUpdatingLocation()
            break
        case .authorizedWhenInUse:
            startUpdatingLocation()
            break
        case .denied:
            //TODO:这里需要强烈的提示
            break
        case .restricted:
            //TODO:这里需要强烈的提示
            break
        case .notDetermined:
            //
            break
        default:
            break
        }
        checkAuth()
        vc?.updateState()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let newLocation = locations.last else {
            return
        }
        
        //先保存老的方向，等一会判断老的和新的方向有什么区别
        self.course = lastLocation?.course
        
        //设定最少误差来决定要不要链接两个点
        var shouldHandle = false
        if let last = lastLocation {
            var coorDelta = 0.0001
            if newLocation.speed > Double(DataManager.shared.debugSpeedHigh) {//20
                coorDelta = Double(DataManager.shared.debugSpeedHighDelta)
            }
            //mid = 8, low = 2
            if newLocation.speed < Double(DataManager.shared.debugSpeedMid), newLocation.speed > Double(DataManager.shared.debugSpeedLow) {
                coorDelta = Double(DataManager.shared.debugSpeedMidDelta)
            }
            if newLocation.speed <= Double(DataManager.shared.debugSpeedLow) {
                coorDelta = Double(DataManager.shared.debugSpeedLowDelta)
            }
            if let lastCrouse = lastCourse {
                let courseDelta:Double = abs(newLocation.course - lastCrouse)
                shouldHandle = shouldHandleLocationAt(coordA: last, coordB:newLocation , coorDelta: coorDelta, courseDelta: courseDelta)
            } else {
                shouldHandle = true
            }
        }
        
        lastLocation = newLocation
        
        
        if isReady == false || shouldUpdateUserPlaceMark == true {
//            print("User Place Mark 需要更新，并且验证是否在国内")
            getGeoCodeOfUser(block: {}, location: newLocation)
            //TODO: 这里需要handle问题，为了在家测试美国的路线，先把判断return掉
            return
        }
        
        if (vc.userState == .recording), shouldHandle == true {
            lastCourse = newLocation.course
            handelLocation(location: newLocation, needFixCoordinate: isNeedFixedCoordinateForChina)
            UserNotificationHandler.shared.request(title: "正在后台更新位置", body: "会持续更新")
        }
    }
    
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        UserNotificationHandler.shared.request(title: "将要暂停位置更新", body: "不知道什么原因，但是会暂停更新")
    }
    
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        UserNotificationHandler.shared.request(title: "继续更新", body: "会在后台更新位置")
    }
    
    
    func shouldHandleLocationAt(coordA: CLLocation, coordB: CLLocation, coorDelta:Double, courseDelta:Double) -> Bool {
        if  courseDelta > Double(DataManager.shared.debugCourse)   {
            return true
        }
        
//        if abs(coordA.coordinate.latitude - coordB.coordinate.latitude) > coorDelta || abs(coordA.coordinate.longitude - coordB.coordinate.longitude) > coorDelta {
//            return true
//        } else {
//            return false
//        }
        return false
    }
    
    //MARK:生成PolyLine
    func handelLocation(location:CLLocation, needFixCoordinate:Bool) {
        var fixedLocation = location
        //火星坐标
        if needFixCoordinate == true {
                fixedLocation = fixedLocation.fixed
        }
        
        DataManager.shared.insertData(locationPlaceMark: userPlaceMark, coordinate: fixedLocation.coordinate, isFixedCoordinate:isNeedFixedCoordinateForChina, mapview: vc.mapView)
    }
    
    
    func removeCurrentPolyLinesAndPins() {
        vc.mapView.removeOverlays(DataManager.shared.polylinesAddedOnMapsFromLoadingRoute)
        //开始前检查地图上是否有Pin，如果有，移除掉
        if DataManager.shared.runtimeAnnotationPins.count > 0 {
            vc.mapView.removeAnnotations(DataManager.shared.runtimeAnnotationPins)
            DataManager.shared.runtimeAnnotationPins.removeAll()
        }
    }
    
    func addCoordsWithSelected(route:CDRoute?) {
        removeCurrentPolyLinesAndPins()
        
        DataManager.shared.resetForStop()

        DataManager.shared.fetchDataWith(route: route)
        vc.mapView.addPolyLinesFrome(coordinatesSection: DataManager.shared.loadedCoordinates2DSections)
        
        if route == nil {
            return
        }
        
        if let firstcoor = DataManager.shared.loadedCoordinates2DSections.first?.first, let lastcoor = DataManager.shared.loadedCoordinates2DSections.last?.last {
            let pinA = CustomPin(pinTitle: "A", pinSubTitle: "", location:firstcoor)
            let pinB = CustomPin(pinTitle: "B", pinSubTitle: "", location: lastcoor)
            DataManager.shared.runtimeAnnotationPins.append(contentsOf: [pinA,pinB])
            vc.mapView.addAnnotations(DataManager.shared.runtimeAnnotationPins)
        }
        
        vc.mapView.setMapToCenter()
    }
    
    
    
    //用户所在位置相关的信息
    func getGeoCodeOfUser(block:@escaping (()->()), location: CLLocation) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placeMarks, err) in
            geocoder.cancelGeocode()
            if let err = err {
                print(err)
            } else {
                if let placeMarks = placeMarks, placeMarks.count > 0 {
                    self.userPlaceMark = placeMarks.first
                    block()
                    let locality = (placeMarks.first?.locality ?? "") + (placeMarks.first?.subLocality ?? "")
                        self.GetGeoCodeForAreaWith(name: locality)
                }
            }
        }
    }
    
    //当前城市相关的信息,目前只是用来做动画的
    private func GetGeoCodeForAreaWith(name:String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(name, completionHandler: { (rpms, err) in
            geocoder.cancelGeocode()
            if let err = err {
                print(err)
            } else {
                if let pms = rpms, pms.count > 0  {
                    self.cityPlaceMark = pms.first
//                    if let l = pms.first?.location {
//                        self.vc.mapView?.animateMapViewFor(coordinate: l.coordinate, delta: 0.3)
//                    }
                }
            }
        })
    }
    
    func prparePlaceMarkInfo() -> [String:String] {
        var info = [String:String]()
        if let cityPlaceMark = self.userPlaceMark {
            if let cont = cityPlaceMark.country {
                info["Country"] = cont
            }
            
            if let adminArea = cityPlaceMark.administrativeArea {
                info["AdministrativeArea"] = adminArea
            }
            
            if let loc = cityPlaceMark.locality {
                info["Locality"] = loc
            }
            
            if let subLocality = cityPlaceMark.subLocality {
                info["SubLocality"] = subLocality
            }
            
            if let name = cityPlaceMark.name {
                info["Name"] = name
            }
            
            if let subAdmin = cityPlaceMark.subAdministrativeArea {
                info["SubAdministrativeArea"] = subAdmin
            }
            
            if let isoCountryCode = cityPlaceMark.isoCountryCode {
                info["ISOCountryCode"] = isoCountryCode
            }
            
            if let areasOfInterest = cityPlaceMark.areasOfInterest {
                info["Areas Of Interest"] = areasOfInterest.joined(separator: ", ")
            }
            
            if let course = self.course {
                let di = String(course)
                info["Direction"] = di
            }
            
            if let speed = self.lastLocation?.speed {
                let s = String(speed)
                info["Speed"] = s
            }
            
            info["Coords Count: "] = String(DataManager.shared.runtimeCoordinates.count)
            
            info["Distance: "] = String(distance)
        }
        
        return info
    }
}
