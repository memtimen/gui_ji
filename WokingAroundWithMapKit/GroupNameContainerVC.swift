//
//  GroupNameContainerVC.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/11.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class GroupNameContainerVC: UIViewController {

    @IBOutlet weak var buttonStart: UIButton!
    var vc:ViewController?
    var groupSelectionTVC:GroupSelectionTVC?
    var textField:UITextField?
    var newGroup:String? {
        didSet {
            if let group = newGroup {
                DataManager.shared.loadedGroupNamesAll.insert(group, at: 0)
                groupSelectionTVC?.prepareDataModel(withInsert:true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(activateStartButton), name: NSNotification.Name("activate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deactivateStartButton), name: NSNotification.Name("deactivate"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    @objc func activateStartButton() {
        print("did activate")
//        buttonStart.isEnabled = true
        vc?.userState = .groupSelected
    }
    
    @objc func deactivateStartButton() {
        print("did deactivate")
//        buttonStart.isEnabled = false
        vc?.userState = .selectGroup
    }
    
    @IBAction func addGroup(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "添加分类", message: nil, preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: "取消", style: .cancel) { (action) in
            
        }
        let actionDone = UIAlertAction(title: "完成", style: .default) { (action) in
            self.newGroup = self.textField?.text
        }
        alert.addAction(actionCancel)
        alert.addAction(actionDone)
        alert.addTextField { (textFeild) in
            self.textField = textFeild
        }
        present(alert, animated: true, completion: nil)
    }
    

    @IBAction func dismiss(_ sender: Any) {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "close")))
        vc?.userState = .stoped
        
    }

    @IBAction func start(_ sender: UIButton) {
        vc?.userState = .recording
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "close")))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "t" {
            groupSelectionTVC = segue.destination as? GroupSelectionTVC
            vc?.groupSelectionTVC = groupSelectionTVC
        }
    }
    
}
