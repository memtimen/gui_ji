//
//  UserNotificationHandler.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/4.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit
import UserNotifications

class UserNotificationHandler: NSObject, UNUserNotificationCenterDelegate {
    
    static let shared = UserNotificationHandler()
    
    override init() {
        super.init()
    }
    
    
    func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            print("notification status:",granted)
        }
    }
    
    
    func request(title:String,body:String) {
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.categoryIdentifier = "alarm"
        content.threadIdentifier = "notices"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default
        //        var dateComponents = DateComponents()
        //        dateComponents.hour = 23
        //        dateComponents.minute = 32
        //        let trigger  =  UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: TimeInterval() + 1, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        center.add(request)
        center.delegate = self
    }
    
    //MARK: delegate
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("will present")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "TestIdentifier" {
            print("handling notifications with the TestIdentifier Identifier")
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        print("open")
    }
}
