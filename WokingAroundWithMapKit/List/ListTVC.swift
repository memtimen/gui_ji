//
//  ListTVC.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/7.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class ListTVC: UITableViewController {
    
    struct Model {
        let title:String?
        let image:UIImage?
        let distance:Double?
        let placeMark:PlaceMark?
        let gourps:String?
        
        struct PlaceMark {
            let area:String
            let locolity:String?
        }
    }
    
    var models = [StructRoute]()
    var vc: ViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

//         Uncomment the following line to display an Edit button in the navigation bar for this view controller.
         self.navigationItem.rightBarButtonItem = self.editButtonItem
//        prepareModel()
    }
    
    func prepareModel(reloadTableview:Bool) {
        models.removeAll()
        models = DataManager.shared.fechAllRoutes()
        if reloadTableview == true {
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return models.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListCell
        
        cell.routeTitle.text = (models[indexPath.row].route.time?.start)?.stringWith(timeStyle: .short)
        cell.routeDistance.text = models[indexPath.row].route.distance.formattedString
        cell.placeMarkLabelArea.text = Array(models[indexPath.row].areas).last
        cell.placeMarkLabelCololity.text = Array(models[indexPath.row].localities).last
        cell.groupLable.text = Array(models[indexPath.row].groups).last
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let route = models[indexPath.row].route {
            LocationManager.shared.addCoordsWithSelected(route: route)
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            if let route = models[indexPath.row].route {
                DataManager.shared.delete(route: route)
            }
            models.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            LocationManager.shared.addCoordsWithSelected(route: nil)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
