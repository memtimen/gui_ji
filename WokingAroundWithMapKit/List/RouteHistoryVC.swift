//
//  RouteHistoryVC.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/24.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class RouteHistoryVC: UIViewController, UIBarPositioningDelegate {

    var vc:ViewController?
    
    var groupSelectonTVC:GroupSelectionTVC?
    
    @IBOutlet weak var viewForPickerView: ViewForPickerView!
    @IBOutlet weak var viewForPickerViewBottomConstrain: NSLayoutConstraint!

    var constantForPickerViewClose:CGFloat = 0

    
    lazy var data = DataManager.shared.prepareDataForList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        viewForPickerView.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateContrains()
        viewForPickerView.alpha = 1
        vc?.listTVC?.prepareModel(reloadTableview: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vc?.whiteStatusBar = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.vc?.buttonStart.alpha = 1
            self.vc?.monitorView.alpha = 1
        }) { (finished) in
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    
    deinit {
        print("RouteHistoryVC denit")
    }
    
    
    func updateContrains() {
        constantForPickerViewClose = -viewForPickerView.frame.height
        viewForPickerViewBottomConstrain.constant = constantForPickerViewClose
    }
    
    
    @IBAction func didTouchUpInsidePlace(_ sender: UIButton) {
        groupSelectonTVC?.view.isHidden = true
        viewForPickerView.setupWith(countryToAreas:  data.countryToAreas, areasToLocolities: data.areasToLocolities, groupOfRoutes: data.groupOfRoutes)
        pickerViewForPlace(show: true)
    }
    
    @IBAction func didTouchUpInsideGoup(_ sender: UIButton) {
        groupSelectonTVC?.view.isHidden = false
        if groupSelectonTVC == nil {
            groupSelectonTVC = GroupSelectionTVC.initGroupSelectonTVCFor(purpose: .sort, groups: data.groupOfRoutes)
            let rect = viewForPickerView.bounds
            groupSelectonTVC?.view.frame = CGRect(x: 0, y: 44, width: rect.width, height: rect.height - 44)
            groupSelectonTVC?.routeHistoryVC = self
            viewForPickerView.addSubview(groupSelectonTVC!.view)
        }
        pickerViewForPlace(show: true)
    }
    
    @IBAction func hidePickerView(_ sender: UIBarButtonItem) {
        pickerViewForPlace(show: false)
    }
    
    
    func pickerViewForPlace(show:Bool) {
        viewForPickerViewBottomConstrain.constant = (show == true) ? 0 : constantForPickerViewClose
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [], animations: {
            self.view.layoutIfNeeded()
        }) { (finish) in
            
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "list" {
            vc?.listTVC = segue.destination as? ListTVC
        }
    }
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }

}
