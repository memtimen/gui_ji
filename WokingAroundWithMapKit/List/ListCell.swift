//
//  ListCell.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/15.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak var routeTitle: UILabel!
    @IBOutlet weak var routeDistance: UILabel!
    @IBOutlet weak var placeMarkLabelArea: UILabel!
    @IBOutlet weak var placeMarkLabelCololity: UILabel!
    @IBOutlet weak var groupLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let sv = UIView(frame: bounds)
        sv.backgroundColor = UIColor(white: 0.25, alpha: 0.8)
        selectedBackgroundView = sv
        // Configure the view for the selected state
    }

}
