//
//  GroupSelectionTVC.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/11.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class GroupSelectionTVC: UITableViewController {
    
    enum Purmpose {
        case record
        case sort
    }
    
    struct DataModel {
        var name:String!
        var isSlected:Bool
    }

    var model = [DataModel]()
    var purpose = Purmpose.record
    var routeHistoryVC:RouteHistoryVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        prepareDataModel()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    class func initGroupSelectonTVCFor(purpose:Purmpose, groups:NSMutableOrderedSet) -> GroupSelectionTVC {
        
        let tvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "groupTVC") as! GroupSelectionTVC
        tvc.purpose = purpose
        if tvc.purpose == .sort {
            let names = groups.array as! [String]
            for name in names {
                let data = DataModel(name: name, isSlected: false)
                tvc.model.append(data)
            }
        }
        tvc.model[0].isSlected = true
        return tvc
    }
    
    func prepareDataModel(withInsert:Bool = false) {
            model.removeAll()
            for data in DataManager.shared.loadedGroupNamesAll.array as! [String] {
                let m = DataModel(name: data, isSlected: false)
                model.append(m)
            }
            if withInsert == true {
                tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            } else {
                tableView.reloadData()
            }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = model[indexPath.row].name
        if model[indexPath.row].isSlected == true {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if purpose == .sort {
            if indexPath.row != 0 {
                
                routeHistoryVC?.vc?.listTVC?.prepareModel(reloadTableview: false)
                if let filtered = routeHistoryVC?.vc?.listTVC?.models.filter({$0.groups.contains(model[indexPath.row].name)}) {
                    routeHistoryVC?.vc?.listTVC?.models = filtered
                    routeHistoryVC?.vc?.listTVC?.tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
                }
            } else {
                //全部
                routeHistoryVC?.vc?.listTVC?.prepareModel(reloadTableview: true)
            }
            for i in 0..<model.count {
                model[i].isSlected = false
            }
            model[indexPath.row].isSlected = !model[indexPath.row].isSlected
            tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .automatic)
            
        } else {
            
            if model[indexPath.row].isSlected == false {
                DataManager.shared.runtimeGroupNames.add(model[indexPath.row].name)
            } else {
                DataManager.shared.runtimeGroupNames.remove(model[indexPath.row].name)
            }
            model[indexPath.row].isSlected = !model[indexPath.row].isSlected
            tableView.reloadRows(at: [indexPath], with: .automatic)
            checkShouldSetStartButtonActive()
        }
    }
    
    //检查开始按钮是否应该可用
    func checkShouldSetStartButtonActive() {
        let selectedGroup = model.filter{ ($0.isSlected == true)}
        if selectedGroup.count > 0 {
            print("可以进行录制了")
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "activate")))
        } else {
            print("没有选择分类，不能进行录制")
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "deactivate")))
        }
    }
}
