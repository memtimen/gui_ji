//
//  DebugTVC.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/13.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class DebugTVC: UITableViewController {

    @IBOutlet weak var sliderHSpeedDelta: UISlider!
    @IBOutlet weak var sliderHSpeed: UISlider!
    @IBOutlet weak var labelHSpeed: UILabel!
    @IBOutlet weak var labelHSpeedDelta: UILabel!
    
    @IBOutlet weak var sliderMSpeed: UISlider!
    @IBOutlet weak var sliderMSpeedDelta: UISlider!
    @IBOutlet weak var labelMSpeed: UILabel!
    @IBOutlet weak var labelMSpeedDelta: UILabel!
    
    @IBOutlet weak var sliderCrouse: UISlider!
    @IBOutlet weak var labelCourse: UILabel!
    
    @IBOutlet weak var sliderSaveFrequnce: UISlider!
    @IBOutlet weak var labelSaveFrequnce: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderCrouse.value = DataManager.shared.debugCourse
        sliderHSpeed.value = DataManager.shared.debugSpeedHigh
        sliderHSpeedDelta.value = DataManager.shared.debugSpeedHighDelta
        sliderMSpeed.value = DataManager.shared.debugSpeedMid
        sliderMSpeedDelta.value = DataManager.shared.debugSpeedMidDelta
        sliderSaveFrequnce.value = DataManager.shared.debbugSaveFrequency
        labelHSpeed.text = String(format: "%.f", DataManager.shared.debugSpeedHigh)
        labelHSpeedDelta.text = String(format: "%.3f", DataManager.shared.debugSpeedHighDelta)
        labelMSpeed.text = String(format: "%.f", DataManager.shared.debugSpeedMid)
        labelMSpeedDelta.text = String(format: "%.4f", DataManager.shared.debugSpeedMidDelta)
        labelCourse.text = String(format: "%.f", DataManager.shared.debugCourse)
        labelSaveFrequnce.text = String(format: "%.f", DataManager.shared.debbugSaveFrequency)
    }

    //High
    @IBAction func highSpeedDidChange(_ sender: UISlider) {
        labelHSpeed.text = String(format: "%.f", sender.value)
    }
    
    
    @IBAction func highSpeedDeltaDidChange(_ sender: UISlider) {
        labelHSpeedDelta.text = String(format: "%.3f", sender.value)
    }
    
    
    //Low
    @IBAction func midSpeedDidChange(_ sender: UISlider) {
        labelMSpeed.text = String(format: "%.f", sender.value)
        DataManager.shared.debugSpeedMid = sender.value
    }
    
    @IBAction func midSpeedDeltaDidChage(_ sender: UISlider) {
        labelMSpeedDelta.text = String(format: "%.4f", sender.value)
        DataManager.shared.debugSpeedMidDelta = sender.value
    }
    
    //Course
    @IBAction func courseDidChange(_ sender: UISlider) {
        labelCourse.text = String(format: "%.f", sender.value)
        DataManager.shared.debugCourse = sender.value
    }
    
    
    //save frequnce
    @IBAction func saveFrequnceDidChange(_ sender: UISlider) {
        labelSaveFrequnce.text = String(format: "%.f", sender.value)
        DataManager.shared.debbugSaveFrequency = sender.value
    }
    
    
}
