//
//  ViewController.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/3.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


enum UserState {
    case selectGroup
    case groupSelected
    case recording
    case stoped
    case prepareForShare
    case readyForShare
    }

class ViewController: UIViewController {
    
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var mapView: MapView! {
        didSet {
            mapView.vc = self
        }
    }
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var containerViewGroupList: UIView!
    @IBOutlet weak var containerViewGroupListBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var debugView: UIView!
    @IBOutlet weak var labelCountry: UILabel!
    @IBOutlet weak var labelPlaceMark: UILabel!
    @IBOutlet weak var labelRoutes: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelGroup: UILabel!
    @IBOutlet weak var sortBar: UIToolbar!
    @IBOutlet weak var monitorView: UIView!
    
    var constantForCloseGroupList:CGFloat = 0
    var constantForCloseHistoryList:CGFloat = 0
    var constantForPickerViewClose:CGFloat = 0
    
    var groupNameContainerVC: GroupNameContainerVC?
    var groupSelectionTVC: GroupSelectionTVC?
    var routeHistoryVC:RouteHistoryVC?
    var listTVC:ListTVC? {
        didSet {
            if oldValue == nil {
                listTVC?.vc = self
            }
        }
    }
    
    var whiteStatusBar = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(closeGroupListContainerView), name: NSNotification.Name("close"), object: nil)
        LocationManager.shared.checkAuth()
        LocationManager.shared.vc = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        containerViewGroupList.alpha = 0
        
        whiteStatusBar = false
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        LocationManager.shared.addCoordsWithSelected(route: nil)
        mapView.addPolyLinesFrome(coordinatesSection: DataManager.shared.loadedCoordinates2DSections)
        mapView.setMapToCenter()
        
        let data = DataManager.shared.fetchPlaceMark()
        let countries = NSSet(array:data.compactMap({$0.name}))
        let area = NSSet(array: data.compactMap({$0.areas}))
        
        
        labelCountry.text = String(format: "\(countries.count)个国家")
        labelPlaceMark.text = String(format: "\(area.count)个地点")
        labelRoutes.text = String(format: "\(DataManager.shared.loadedCoordinates2DSections.count)个轨迹")
        labelDistance.text = String(format: "轨记总长\(DataManager.shared.loadedDistance.formattedString)")
        labelGroup.text = String(format: "\(DataManager.shared.loadedGroupNamesAll.count)个分类")
        
        updateContrains()
        
        containerViewGroupList.alpha = 1
    }
    
    
    var userState = UserState.stoped {
        didSet {
            updateState()
            
            if userState == .stoped {
                UIView.animate(withDuration: 0.3) {
                    self.monitorView.alpha = 1
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.monitorView.alpha = 0
                }
            }
            
            switch userState {
            case .selectGroup:
                buttonStart.isEnabled = false
            case .groupSelected:
                buttonStart.isEnabled = true
            case .stoped:
                buttonStart.isEnabled = true
                buttonStart.setTitle("开始记录轨迹", for: .normal)
                mapView.removeOverlays(DataManager.shared.runtimePolyLines)
                //开始前检查地图上是否有Pin，如果有，移除掉
                if DataManager.shared.runtimeAnnotationPins.count > 0 {
                    mapView.removeAnnotations(DataManager.shared.runtimeAnnotationPins)
                    DataManager.shared.runtimeAnnotationPins.removeAll()
                }
                
                //把地图重置到用户的位置
//                let coor = LocationManager.shared.currenLocation.coordinate
//                mapView.setCenter(coor, animated: true)
                mapView.userTrackingMode = .none
                break
            case .prepareForShare:
                guard let currentLocation = LocationManager.shared.lastLocation  else {
                    return
                }
                //停止时的位置添加进来
                LocationManager.shared.handelLocation(location: currentLocation, needFixCoordinate: LocationManager.shared.isNeedFixedCoordinateForChina)
                
                //点击停止的时候添加第二个pin到地图上
                let pinB = CustomPin(pinTitle: "B", pinSubTitle: "终止点", location: LocationManager.shared.currenLocation.coordinate)
                mapView.addAnnotation(pinB)
                DataManager.shared.runtimeAnnotationPins.append(pinB)
                mapView.setMapRectForShare()
                //保存数据
                DataManager.shared.prepareDataWith(locationPlaceMark: LocationManager.shared.userPlaceMark)
                
                mapView.userTrackingMode = .none
                self.userState = .readyForShare
                buttonStart.setTitle("完成", for: .normal)
                break
            case .readyForShare:
                buttonStart.isEnabled = true
                buttonStart.setTitle("完成", for: .normal)
                break
            case .recording:
                buttonStart.setTitle("停止记录轨迹", for: .normal)
                DataManager.shared.startDate = Date()
                if LocationManager.shared.lastLocation != nil {
                    LocationManager.shared.handelLocation(location: LocationManager.shared.lastLocation!, needFixCoordinate: LocationManager.shared.isNeedFixedCoordinateForChina)
                }
                mapView.userTrackingMode = .follow
                //点击开始的时候添加第一个Pin地图上
                let pinA = CustomPin(pinTitle: "A", pinSubTitle: "起始点", location: LocationManager.shared.currenLocation.coordinate)
                mapView.addAnnotation(pinA)
                DataManager.shared.runtimeAnnotationPins.append(pinA)
                break
                
            }
        }
    }
    

    
    func updateContrains() {
        constantForCloseGroupList = -containerViewGroupList.frame.height - containerViewGroupListBottomConstraint.constant - view.safeAreaInsets.bottom
        containerViewGroupListBottomConstraint.constant = constantForCloseGroupList
    }
    
    func updateState() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .denied,.restricted, .notDetermined:
            buttonStart.setTitle("需要开启定位，去开启定位", for: .normal)
            break
        case .authorizedAlways:
            if userState == .stoped {
                buttonStart.setTitle( "开始记录轨迹", for: .normal)
            }
            break
        case .authorizedWhenInUse:
            if userState == .stoped {
            buttonStart.setTitle( "开始记录轨迹", for: .normal)
                //需要提示要开启使用允许
            }
            break
        default:
            break
        }
    }
    
    @IBAction func tapOnInfoView(_ sender: UITapGestureRecognizer) {
            self.setNeedsStatusBarAppearanceUpdate()
        buttonStart.setTitle("关闭轨迹列表", for: .normal)
        whiteStatusBar = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.buttonStart.alpha = 0
            self.monitorView.alpha = 0
        }) { (finished) in
            self.performSegue(withIdentifier: "list", sender: nil)
        }
    }
    
    

    @IBAction func startButtonDidPressed(_ sender: UIButton) {
        let status = CLLocationManager.authorizationStatus()
        
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            switch userState {
            case .selectGroup:
                break
            case .groupSelected:
                userState = .recording
                closeGroupListContainerView()
                break
            case .recording:
                userState = .prepareForShare
                break
            case .prepareForShare:
                break
            case .readyForShare:
                userState = .stoped
                //把当前内存中的数据都清空
                DataManager.shared.resetForStop()
                break
            case .stoped:
//                userState = .recording
                if LocationManager.shared.isReady {
                    showGroupListContainerView()
                }
                userState = .selectGroup
                break
            }
            break
        case .denied, .restricted, .notDetermined:
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            UIApplication.shared.open(settingsAppURL, options: [:]) { (ok) in}
            break
        default:
            break
        }
    }
    
    @IBAction func debugView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, animations: {
            self.debugView.alpha = self.debugView.alpha == 0 ? 1:0
        }) { (finish) in
            
        }
    }
    
    
    
    @IBAction func unwind(sender: UIStoryboardSegue) {
        
    }
    
    
    @objc func closeGroupListContainerView() {
        print("close")
        containerViewGroupListBottomConstraint.constant = constantForCloseGroupList
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 2, options: [], animations: {
            self.view.layoutIfNeeded()
        }) { (finish) in
            
        }
    }
    
    
    func showGroupListContainerView() {
        groupSelectionTVC?.prepareDataModel()
        groupSelectionTVC?.tableView.reloadData()
        
        containerViewGroupListBottomConstraint.constant = 90
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 2, options: [], animations: {
            self.view.layoutIfNeeded()
        }) { (finish) in
            
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nav" {
            let nv = segue.destination as? UINavigationController
            let cv = nv?.viewControllers.first as? GroupNameContainerVC
            cv?.vc = self
            groupSelectionTVC = cv?.groupSelectionTVC
        }
        
        if segue.identifier == "list" {
            if let nav = segue.destination as? UINavigationController {
                routeHistoryVC = nav.viewControllers.first as? RouteHistoryVC
                routeHistoryVC?.vc = self
            }
        }
        
    }
    
    func updateDebugView(calledFrom:String) {
        var info =  LocationManager.shared.prparePlaceMarkInfo()
        info["Called With"] = calledFrom
        let text = info.description.replacingOccurrences(of: ",", with: "\n")
        textView.text = text
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if whiteStatusBar == false {
           return .default
        } else {
            return .lightContent
        }
    }
    
}

