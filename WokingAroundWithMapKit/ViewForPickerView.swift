//
//  viewForPickerView.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/24.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit

class ViewForPickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource, UIBarPositioningDelegate {

    var pickerView:UIPickerView?
    
    struct Data {
        var country:[String]
        var area:[String]
        var locolity:[String]
    }
    
    var countryToAreas = [[String:[String]]]()
    var areasToLocolities = [[String:[String]]]()
    
    
    var countryNames = ["—"] {
        didSet {
            if countryNames.first != "—" {
                countryNames.insert("—", at: 0)
            }
        }
    }
    var areaNames = ["—"] {
        didSet {
            if areaNames.first != "—" {
                areaNames.insert("—", at: 0)
            }
        }
    }
    var locolitiesNames = ["—"] {
        didSet {
            if locolitiesNames.first != "—" {
                locolitiesNames.insert("—", at: 0)
            }
        }
    }
    
    var data:Data!
    
    
    
    func setupWith(countryToAreas: [[String : [String]]], areasToLocolities: [[String : [String]]], groupOfRoutes: NSMutableOrderedSet) {
        self.countryToAreas = countryToAreas
        self.areasToLocolities = areasToLocolities
        
        let d = data(com: 0, row: 0)
        countryNames = d.countries
        areaNames = d.areas
        locolitiesNames = d.locolities
        
        data = Data(country: countryNames, area: areaNames, locolity: locolitiesNames)
        
        if pickerView == nil {
            pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height))
            pickerView?.showsSelectionIndicator = true
            pickerView?.delegate = self
            pickerView?.dataSource = self
            insertSubview(pickerView!, at: 0)
        }
    }
    
    
    
    
    
    //MARK: Picker
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return data.country.count
        case 1:
            return data.area.count
        case 2:
            return data.locolity.count
        default:
            return 1
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var titleData = ""
        switch (component,row) {
        case (0,row):
            titleData = data.country[row]
        case (1,row):
            titleData = data.area[row]
        case (2,row):
            titleData = data.locolity[row]
        default:
            titleData = "a ohhhhh"
        }
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return myTitle
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let d = data(com: component, row: row)
        print("did select row:", row, "component: ",component,"data:", d)
        data.country = d.countries
        data.area = d.areas
        data.locolity = d.locolities
        
        if component == 0 {
            pickerView.reloadComponent(1)
            pickerView.reloadComponent(2)
            pickerView.selectRow(0, inComponent: 1, animated: true)
        }
        
        if component == 1 {
            pickerView.reloadComponent(2)
            pickerView.selectRow(0, inComponent: 2, animated: true)
        }
    }
    
    
    func data(com: Int, row:Int) -> (countries:[String],areas:[String],locolities:[String]){
        print("will prepare data for com: \(com), row: \(row)")
        
        if data == nil {
            let dicC = countryToAreas[0]
            let key = Array(dicC.keys)
            countryNames = key
            return (countryNames, areaNames, locolitiesNames)
        }
        
        
        if com == 0 {
            let dicCountry = countryToAreas[0]
            let keys = Array(dicCountry.keys)
            countryNames = keys
            let key = countryNames[row]
            print("key",key)
            
            
            
            let dicAarea = countryToAreas[com]
            if let values = dicAarea[key] {
                //                areaNames.removeAll()
                areaNames = values
            } else {
                areaNames = ["—"]
            }
            
            locolitiesNames = ["—"]
            
        }
        
        
        if com == 1 {
            let key = data.area[row]
            let dicLocolities = areasToLocolities[0]
            if let  values = dicLocolities[key] {
                locolitiesNames = values
            } else {
                locolitiesNames = ["—"]
            }
        }
        
        
        if com == 2 {
            
        }
        
        return (countryNames, areaNames, locolitiesNames)
    }
    
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }

}
