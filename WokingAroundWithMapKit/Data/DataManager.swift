//
//  DataManager.swift
//  WokingAroundWithMapKit
//
//  Created by Charlie Freeman on 2018/11/7.
//  Copyright © 2018 Maimaitiming Abudukadier. All rights reserved.
//

import UIKit
import CoreData
import MapKit


struct StructRoute {
    var groups: Set<String>!
    var startDate:Date!
    var endDate:Date!
    var localities:Set<String>!
    var areas:Set<String>!
    var countries:Set<String>!
    var coordinate:[Coordinate]!
    var route:CDRoute!
    
    struct Coordinate: Decodable, Encodable {
        let lat:Double!
        let lon:Double!
    }
}

struct StructTime {
    let start:Date!
    let end:Date!
    let route:StructRoute!
}

struct StructPlaceMark {
    var country:County
    var area:AdminArea
    var locolity:Locolity
    
    struct County {
        var names:Set<String>
    }
    
    struct AdminArea {
        var names:Set<String>
    }
    
    struct Locolity {
        var names:Set<String>
    }
}


class DataManager: NSObject {

    static let shared = DataManager()
    
    var startDate:Date!
    
    //runtime相关的都是App在运行中，用来记录数据的，在内存中的内容
    var runtimeCoordinates = [CLLocationCoordinate2D]() {
        didSet {
            print("runtime Coordinates counts: ",runtimeCoordinates.count)
        }
    }
    var runtimeGroupNames = NSMutableOrderedSet()
    var runtimeDistance:Double = 0
    var runtimePolyLines = [MKOverlay]()
    var runtimeRouteImage:UIImage?
//    var runtimePlaceMark:StructPlaceMark?
    var runtimeCountryObjects = [CDRouteCountry]()
    var runtimePlaceMarkCountryName = Set<String>()
    var runtimePlaceMarkAdminAreaName = Set<String>()
    var runtimePlaceMarkLocolilyName = Set<String>()
    var curentRoute:StructRoute?
    
    
    //记录下来，完成后需要移除的时候用
    var runtimeAnnotationPins = [CustomPin]()
    //从选择的route中添加的Polyline，为了到时候能删掉，所以只好留住
    var polylinesAddedOnMapsFromLoadingRoute = [MKPolyline]()
    
    //这是拿去让Mapview加载的数据，可以换一个名字
    var loadedCoordinates2DSections = [[CLLocationCoordinate2D]]()
    
//    var loadedRouts = [StructRoute]()
    var loadedGroupNamesAll = NSMutableOrderedSet()
    
    var loadedDistance = Double()
    
    var routeObject:CDRoute?
    var timeObject:CDRouteTime?
    var coordObjects = [CDRouteCoordinate]()
    var coordObject = CDRouteCoordinate()
    //这个是跟Route有关的，里面有route
    var routeGroupObjects = [CDRouteGroup]()
    var routeGroupObject:CDRouteGroup?
    
    var cSet = [CDRouteCountry]()
    var aSet = [CDRouteArea]()
    var lSet = [CDRouteLocolily]()
    
    //这个是Group列表用的
    var groupEntityObjects = [CDGroupEntity]()
    var groupEntity:CDGroupEntity?
    
    var debbugSaveFrequency:Float = 5
    var debugSpeedHigh:Float = 20
    var debugSpeedMid:Float = 8
    var debugSpeedLow:Float = 2
    var debugCourse:Float = 2
    var debugSpeedHighDelta:Float = 0.0005
    var debugSpeedMidDelta:Float = 0.0003
    var debugSpeedLowDelta:Float = 0.0001
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "coreDataPlayGround")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    
    //MARK:整理数据内容Location Manager did updte的时候实时更新，不能有数据延迟和跳过的情况
    func insertData(locationPlaceMark:CLPlacemark?, coordinate:CLLocationCoordinate2D, isFixedCoordinate:Bool, mapview:MapView) {
        let _ = DataManager.shared.handle(locationPlaceMark: locationPlaceMark)
        runtimeCoordinates.append(coordinate)
        let polyLine = MKPolyline(coordinates: &DataManager.shared.runtimeCoordinates, count: DataManager.shared.runtimeCoordinates.count)
        polyLine.title = "runTime"
        mapview.addOverlay(polyLine)
        
        //写入的频率
        if runtimeCoordinates.count % Int(debbugSaveFrequency) == 0 || runtimeCoordinates.count == 0 {
            prepareDataWith(locationPlaceMark: nil)
        }
    }
    
    
    func prepareDataWith(locationPlaceMark:CLPlacemark?) {
        if let locationPlaceMark = locationPlaceMark {
            let _ = DataManager.shared.handle(locationPlaceMark: locationPlaceMark)
        }
        updateRuntimeData()
    }
    
    
    
    func updateRuntimeData() {
        
        //Coordinates
        coordObjects.removeAll() //Clean up current Coordinates first
        for coor in runtimeCoordinates.suffix(Int(debbugSaveFrequency)) {
            coordObject = CDRouteCoordinate(context: persistentContainer.viewContext)
            coordObject.latitude = coor.latitude
            coordObject.longitude = coor.longitude
            coordObjects.append(coordObject)
        }
        
        //Date
        if timeObject == nil {
            timeObject = CDRouteTime(context: persistentContainer.viewContext)
        }
        timeObject?.start = self.startDate
        timeObject?.end = Date()
        
        //Route
        if routeObject == nil {
            routeObject = CDRoute(context: persistentContainer.viewContext)
        }
        
        routeObject?.distance = runtimeDistance
        routeObject?.addToCoordinates(NSOrderedSet(array: coordObjects))
        routeObject?.time = timeObject
        
        
        //Groups 将选择的Group写入到Route中
        if let rumtimeGroupsArray = runtimeGroupNames.array as? [String] {
            for name in rumtimeGroupsArray {
                if let  routeGroups = routeObject?.groups?.allObjects as? [CDRouteGroup] {
                    if !routeGroups.compactMap({$0.name}).contains(name) {
                        loadedGroupNamesAll.insert(name, at: 0)
                        routeGroupObject = CDRouteGroup(context: persistentContainer.viewContext)
                        routeGroupObject?.name = name
                        routeGroupObjects.append(routeGroupObject!)
                    }
                }
            }
            routeObject?.groups = NSSet(array: routeGroupObjects)
        } else {
            print("这个不该发生，如果发生了，那就是有问题，需要检查")
        }
  
        //保存用户添加的Group
        for name in loadedGroupNamesAll.array as! [String] {
            if !loadedGroupNamesAll.contains(name) {
                groupEntity = CDGroupEntity(context: persistentContainer.viewContext)
                groupEntity!.name = name
                groupEntityObjects.insert(groupEntity!, at: 0)
                print("即将保存的gourp name：",name)
            }
        }
        
        //添加RouteImage
        if runtimeRouteImage != nil {
            let imageEntity = CDRouteImage(context: persistentContainer.viewContext)
            imageEntity.image = runtimeRouteImage
            routeObject?.image = imageEntity
        }
        
        //Place Mark
        runtimeCountryObjects = fetchPlaceMark()
        
        
        if let data = handle(locationPlaceMark: LocationManager.shared.userPlaceMark) {

            //Locolity变了
            if !runtimePlaceMarkLocolilyName.contains(data.locolity) {
                runtimePlaceMarkLocolilyName.insert(data.locolity)
                let locolityObject = CDRouteLocolily(context: persistentContainer.viewContext)
                locolityObject.name = data.locolity
                lSet.append(locolityObject)
                print("添加了 Locolity")
                
                //Area变了
                if !runtimePlaceMarkAdminAreaName.contains(data.adminArea) {
                    runtimePlaceMarkAdminAreaName.insert(data.adminArea)
                    let areaObject = CDRouteArea(context: persistentContainer.viewContext)
                    areaObject.name = data.adminArea
                    aSet.append(areaObject)
                    print("添加了 Area")
                }
                
                //国家变化了,没有国家 -> 有国家
                if !runtimePlaceMarkCountryName.contains(data.country) {
                    runtimePlaceMarkCountryName.insert(data.country)
                    let countryObject = CDRouteCountry(context: persistentContainer.viewContext)
                    countryObject.name = data.country
                    cSet.append(countryObject)
                    print("添加了 country")
                }
                
                aSet.last?.locolilies = NSSet(array: lSet)
                cSet.last?.areas = NSSet(array: aSet)
                
                routeObject?.locolities = NSSet(array: lSet)
                routeObject?.adminAreas = NSSet(array: aSet)
                routeObject?.countries = NSSet(array: cSet)
            }
            
        }
        
        saveContext()
    }
    
    
    
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("saved")
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func fetchDataWith(route:CDRoute?) {
        var routes = [CDRoute]()
        
        if route == nil {
            let routesRequest = CDRoute.fetchRequest() as NSFetchRequest<CDRoute>
            routes = try! persistentContainer.viewContext.fetch(routesRequest) as [CDRoute]
        } else {
            routes.append(route!)
        }
        loadedDistance = Double()
        var coors2D = [CLLocationCoordinate2D]()
        for route in routes {
            loadedDistance += route.distance
            if let corrdinates = route.coordinates?.array as? [CDRouteCoordinate] {
                for coor in corrdinates {
                    coors2D.append(CLLocationCoordinate2D(latitude: coor.latitude, longitude: coor.longitude))
                }
            }
            loadedCoordinates2DSections.append(coors2D)
            coors2D.removeAll()
        }
        
    }
    
    //给listview提供可能会用到的数据
    func prepareDataForList() -> (countryToAreas: [[String:[String]]] ,areasToLocolities: [[String:[String]]],groupOfRoutes:NSMutableOrderedSet) {
        let routesRequest = CDRoute.fetchRequest() as NSFetchRequest<CDRoute>
        let routes = try! persistentContainer.viewContext.fetch(routesRequest) as [CDRoute]
        
        var groupNames = NSMutableOrderedSet()
        
        var countryToAreas = [[String:[String]]]()
        var areasToLocolities = [[String:[String]]]()
        
        var caDic = [String:[String]]()
        var alDic = [String:[String]]()
        
        
        var countryNames = Set<String>()
        for route in routes {
            if let groups = route.groups?.allObjects as? [CDRouteGroup] {
                for group in groups {
                    if let name = group.name {
                        groupNames.add(name)
                    }
                }
            }
            groupNames.insert("全部", at: 0)
            
            //country
            if let countries = route.countries?.allObjects as? [CDRouteCountry] {
                for country in countries {
                    var countrySecion = [String:[String]]()
                    if let countryName = country.name {
                        countryNames.insert(countryName)
                        
                        //areas
                        var areaNames = Set<String>()
                        if let areas = country.areas?.allObjects as? [CDRouteArea] {
                            for area in areas {
                                if let areaName = area.name {
                                    areaNames.insert(areaName)
                                    
                                    //locolities
                                    var locolityNames = Set<String>()
                                    if let locolities = area.locolilies?.allObjects as? [CDRouteLocolily] {
                                        for locolity in locolities {
                                            if let name = locolity.name {
                                                locolityNames.insert(name)
                                            }
                                        }
                                        
                                        if alDic.contains(where: {$0.key == areaName}) {
                                            if let lArray = alDic[areaName] {
                                                let all = locolityNames.union(Set(lArray))
                                                alDic[areaName] = Array(all)
                                            }
                                            
                                        } else {
                                            alDic[areaName] = Array(locolityNames)
                                        }
                                    }
                                }
                                
                                //检查时候有过这个国家
                                if caDic.contains(where: {$0.key == countryName}) {
                                    if let areasArray =  caDic[countryName] {
                                        let set = Set(areasArray)
                                        let all = areaNames.union(set)
                                        caDic[countryName] = Array(all)
                                    }
                                } else {
                                    caDic[countryName] = Array(areaNames)
                                }
                            }
                            countrySecion[countryName] = Array(areaNames)
                        }
                    }
                }
            }
        }
        countryToAreas.append(caDic)
        areasToLocolities.append(alDic)
        print(countryToAreas, areasToLocolities)
        
        return (countryToAreas, areasToLocolities,groupNames)
    }
    
    
    func fetchPlaceMark() -> [CDRouteCountry] {
        let countryRequest = CDRouteCountry.fetchRequest() as NSFetchRequest<CDRouteCountry>
        let countries  = try! persistentContainer.viewContext.fetch(countryRequest)
        return countries
    }
    
    func fechAllRoutes() -> [StructRoute] {
        
        var routes = [StructRoute]()
        let routesRequest = CDRoute.fetchRequest() as NSFetchRequest<CDRoute>
        let routeObjs = try! persistentContainer.viewContext.fetch(routesRequest) as [CDRoute]
        
        for routeObj in routeObjs {
            routes.append(fech(route: routeObj))
        }
        return routes
    }
    
    
    private func fech(route:CDRoute) -> StructRoute {
        var groups = Set<String>()
        if let gs = route.groups?.allObjects as? [CDRouteGroup] {
            for g in gs {
                if let name = g.name {
                    groups.insert(name)
                }
            }
        }
        
        //Time
        let startDate = route.time?.start ?? Date()
        let endDate = route.time?.end ?? Date()
        
        //PlaceMark
        var placeMarkCountries = Set<String>()
        var placeMarkAreas = Set<String>()
        var placeMarkLocalities = Set<String>()
        
        if let ls = route.locolities?.allObjects as? [CDRouteLocolily] {
            for l in ls {
                if let name = l.name {
                    placeMarkLocalities.insert(name)
                }
            }
        }
        
        if let areas = route.adminAreas?.allObjects as? [CDRouteArea] {
            for area in areas {
                if let name = area.name {
                    placeMarkAreas.insert(name)
                }
            }
        }
        
        if let countries = route.countries?.allObjects as? [CDRouteCountry] {
            for country in countries {
                if let name = country.name {
                    placeMarkCountries.insert(name)
                }
            }
        }

        //Coords
        var coors2D = [StructRoute.Coordinate]()
        if let corrdinates = route.coordinates?.array as? [CDRouteCoordinate] {
            for coor in corrdinates {
                coors2D.append(StructRoute.Coordinate(lat: coor.latitude, lon: coor.longitude))
            }
        }
        
        let route = StructRoute(groups: groups, startDate: startDate, endDate: endDate, localities: placeMarkLocalities, areas: placeMarkAreas, countries: placeMarkCountries, coordinate: coors2D, route:route)
        
        return route
    }
    
    
    
    func loadSavedGroups() {
        let savedGroupNamesRequest = CDGroupEntity.fetchRequest() as NSFetchRequest<CDGroupEntity>
        let savedGroups = try! persistentContainer.viewContext.fetch(savedGroupNamesRequest) as [CDGroupEntity]
        for group in savedGroups {
            if let name = group.name {
                loadedGroupNamesAll.insert(name, at: loadedGroupNamesAll.count)
                print("loaded Group name:",name)
            }
        }
        
        //Groups
        if loadedGroupNamesAll.count == 0 {
            loadedGroupNamesAll = [
                "自驾游",
                "骑行",
                "跑步",
                "周末出行记"
            ]
        }
    }
    
    
    func delete(route:CDRoute) {
        persistentContainer.viewContext.delete(route)
        
        saveContext()
    }
    
    
    //从数据库获取Time
    func fechTimeSection() -> [CDRouteTime]? {
        let timesRequest = CDRouteTime.fetchRequest() as NSFetchRequest<CDRouteTime>
        if let times = try? persistentContainer.viewContext.fetch(timesRequest) {
            return times
        }
        return nil
    }
    
    
    func handle(locationPlaceMark:CLPlacemark?) -> (country:String, adminArea:String, locolity:String)? {
        
        guard let placemark = locationPlaceMark else {
            return nil
        }
        
        var data = [String]()
        var da = "🤷🏻‍♂️!?!"
        var db = "🤷🏻‍♂️!?!"
        var dc = "🤷🏻‍♂️!?!"
        
        if let country = placemark.country {
            data.append(country)
        }
        
        if let administrativeArea = placemark.administrativeArea {
            data.append(administrativeArea)
        }
        
        if let subadministrativeArea = placemark.subAdministrativeArea {
            data.append(subadministrativeArea)
        }
        
        if let locality = placemark.locality {
            data.append(locality)
        }
        
        if let sublocality = placemark.subLocality {
            data.append(sublocality)
        }
        
        if let ocean = placemark.ocean {
            data.append(ocean)
        }
        
        data.append("🤷🏻‍♂️")
        
        for i in 0..<data.count {
            switch i {
            case 0:
                da = data[i]
            case 1:
                db = data[i]
            case 2:
                dc = data[i]
            default:
                break
            }
        }
        return (da,db,dc)
    }
    
    
    func resetForStop() {
        startDate = nil
        
        //runtime相关的都是App在运行中，用来记录数据的，在内存中的内容
        runtimeCoordinates = [CLLocationCoordinate2D]()
        runtimeGroupNames = NSMutableOrderedSet()
        runtimeDistance = 0
        runtimePolyLines = [MKOverlay]()
        curentRoute = nil
        runtimeCountryObjects = [CDRouteCountry]()
        runtimePlaceMarkCountryName = Set<String>()
        runtimePlaceMarkAdminAreaName = Set<String>()
        runtimePlaceMarkLocolilyName = Set<String>()
        runtimeAnnotationPins = [CustomPin]()
        polylinesAddedOnMapsFromLoadingRoute = [MKPolyline]()
        
        cSet =  [CDRouteCountry]()
        aSet =  [CDRouteArea]()
        lSet =  [CDRouteLocolily]()
        
        //是拿去让Mapview加载的数据，可以换一个名字
        loadedCoordinates2DSections = [[CLLocationCoordinate2D]]()
        loadedDistance = Double()
//        loadedRouts = [StructRoute]()
        //        loadedGroupNames = NSMutableOrderedSet()
        
        
        routeObject = nil
        timeObject = nil
        coordObjects = [CDRouteCoordinate]()
        coordObject = CDRouteCoordinate()
        routeGroupObjects = [CDRouteGroup]()
        routeGroupObject = nil
        runtimeRouteImage = nil
        
        groupEntityObjects = [CDGroupEntity]()
        groupEntity = nil
        
    }
}
